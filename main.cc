#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>

using namespace std;

void boardGeneration(vector<vector<int> >&, int);
int totalConflictingCount(vector<vector<int> >&, int);
int conflictingCount(vector<vector<int> >&, int, int, int);

int main(int argc, char* argv[]) {
	int choice = 0;
	int totconflicting = 0;
	int tempconflicting = 0;

	cout << "What size queen problem would you like to solve?" << endl;
	cin >> choice;

	vector < vector<int> > generatedboard(choice, vector<int>(choice, 0));
	vector < vector<int> > realboard(choice, vector<int>(choice, 0));
	vector < vector<int> > tempboard(choice, vector<int>(choice, 0));
	vector < vector<int> > fullLoopBoard(choice, vector<int>(choice, 0));

	boardGeneration(generatedboard, choice);
	realboard = generatedboard;

	totconflicting = totalConflictingCount(generatedboard, choice);
	tempconflicting = totconflicting;


	generatedboard = realboard;
	cout << "totconflicting: " << totconflicting << endl;
	int tempconflict2 = 0;
	int duplicateBoard = 0;
	bool madeChanges = true;

	tempboard = realboard;
	fullLoopBoard = realboard;
	while (madeChanges) {
		madeChanges = false;
		for (int qq = 0; qq < choice; qq++) {
			for (int ww = 0; ww < choice; ww++) {
				cout << generatedboard[qq][ww] << " ";
			}
			cout << endl;
		}
		fullLoopBoard = generatedboard;
		for (int q = 0; q < choice; q++) {
			for (int w = 0; w < choice; w++) {
				//outputs steps
				if (generatedboard[q][w] == 1) {
					//checks middle(both left and right up and down)
					if (w + 1 < choice) {
						if (generatedboard[q][w + 1] == 0) {
							tempboard[q][w] = 0;
							tempboard[q][w + 1] = 1;
							tempconflict2 = totalConflictingCount(tempboard,
								choice);
							if (tempconflict2 < tempconflicting) {
								cout << "Moved right" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else if (tempconflict2 == tempconflicting) {
								cout << "Moved right" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else {
								tempboard = generatedboard;
							}
						}
					}
					if (w - 1 >= 0) {
						if (generatedboard[q][w - 1] == 0) {
							tempboard[q][w] = 0;
							tempboard[q][w - 1] = 1;
							tempconflict2 = totalConflictingCount(tempboard,
								choice);
							if (tempconflict2 < tempconflicting) {
								cout << "Moved left" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else if (tempconflict2 == tempconflicting) {
								cout << "Moved left" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else {
								tempboard = generatedboard;
							}
						}
					}
					if (q - 1 >= 0) {
						if (generatedboard[q - 1][w] == 0) {
							tempboard[q][w] = 0;
							tempboard[q - 1][w] = 1;
							tempconflict2 = totalConflictingCount(tempboard,
								choice);
							if (tempconflict2 < tempconflicting) {
								cout << "Moved up" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else if (tempconflict2 == tempconflicting) {
								cout << "Moved up" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else {
								tempboard = generatedboard;
							}
						}
					}	//middle changing
					if (q + 1 < choice) {
						if (generatedboard[q + 1][w] == 0) {
							tempboard[q][w] = 0;
							tempboard[q + 1][w] = 1;
							tempconflict2 = totalConflictingCount(tempboard,
								choice);
							if (tempconflict2 < tempconflicting) {
								cout << "Moved down" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else if(tempconflict2 == tempconflicting) {
								cout << "Moved down" << endl;
								generatedboard = tempboard;
								tempconflicting = tempconflict2;
								madeChanges = true;
							}
							else{
								tempboard = generatedboard;
							}
						}
					}
				}
			}
		}
		if (fullLoopBoard == generatedboard) {
			duplicateBoard++;
		}
		if (tempconflicting == 0 || duplicateBoard > 5) {
			cout << "enter if at end" << endl;
			madeChanges = false;
		}
	}
	cout << "---------------------------------------------------" << endl;
	for (int qq = 0; qq < choice; qq++) {
		for (int ww = 0; ww < choice; ww++) {
			cout << generatedboard[qq][ww] << " ";
		}
		cout << endl;
	}
	cout << "RTC: " << tempconflicting;
	return 0;
}

int totalConflictingCount(vector<vector<int> >& generatedboard, int choice) {
	int horizonal = 0;
	int vertical = 0;
	int totalHorizontal = 0;
	int totalVertical = 0;
	int totalconflicting = 0;

	for (int x = 0; x < choice; x++) {
		for (int w = 0; w < choice; w++) {
			if (generatedboard[x][w] == 1) {
				horizonal = horizonal + 1;
			}
		}
		if (horizonal > 1) {
			totalHorizontal = totalHorizontal + (horizonal - 1);
		}
		horizonal = 0;
	}
	for (int x = 0; x < choice; x++) {
		for (int w = 0; w < choice; w++) {
			if (generatedboard[w][x] == 1) {
				vertical = vertical + 1;
			}
		}
		if (vertical > 1) {
			totalVertical = totalVertical + (vertical - 1);
		}
		vertical = 0;
	}
	totalconflicting = totalVertical + totalHorizontal;
	//cout << totalconflicting << endl;
	return totalconflicting;
}

int conflictingCount(vector<vector<int> >& generatedboard, int choice, int row,
	int col) {
	int horizonal = 0;
	int vertical = 0;

	int totalconflicting = 0;

	for (int x = 0; x < choice; x++) {
		if (generatedboard[row][x] == 1) {
			horizonal = horizonal + 1;
		}
	}
	for (int w = 0; w < choice; w++) {
		if (generatedboard[w][col] == 1) {
			vertical = vertical + 1;
		}
	}
	totalconflicting = (vertical - 1) + (horizonal - 1);
	//cout << totalconflicting << endl;
	return totalconflicting;
}

void boardGeneration(vector<vector<int> >& generatedboard, int choice) {

	//INITIALIZES THE ARRAY OF NQUEEN(PASSED) AND THE RANDOM VALUES
	int r1 = 0;
	int r2 = 0;
	int x = 0;

	// SETS ENTIRE ARRAY VALUES TO 0
	for (int q = 0; q < choice; q++) {
		for (int w = 0; w < choice; w++) {
			generatedboard[q][w] = 0;
		}
		cout << endl;
	}

	//GENERATES THE RANDOM ARRAY OF NQUEENS
	while (x < choice) {

		srand(time(NULL)); r1 = rand() % choice;
		r2 = rand() % choice;
		if (generatedboard[r1][r2] == 0) {
			generatedboard[r1][r2] = 1;
			x++;
		}
	}
	//OUTPUTS THE BOARD
	for (int q = 0; q < choice; q++) {
		for (int w = 0; w < choice; w++) {
			cout << generatedboard[q][w] << " ";
		}
		cout << endl;
	}
}



